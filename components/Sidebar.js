import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";

const Sidebar = () => {
  // routing de next
  const router = useRouter();

  // console.log(router.pathname)

  return (
    <>
      <aside className="bg-indigo-800 sm:w-1/5 xl:w-1/5 sm:min-h-screen p-5">
        <div>
          <p className="text-white text-2xl font-black">SYSTEM ROSSY</p>
        </div>

        <nav className="mt-5 list-none">
          <li className={router.pathname === "/" ? "bg-gray-800 p-2 " : "p-2"}>
            <Link href="/">
              <a className="text-white block">Clientes</a>
            </Link>
          </li>
          
        </nav>
        
      </aside>
    </>
  );
};

export default Sidebar;
