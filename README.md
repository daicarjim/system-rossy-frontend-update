## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)

### General Info
***
System Rossy es un software para poder administrar clientes (con posibilidad de poder ampliarlo a productos y envios). 
### Screenshot
![Image text](https://gitlab.com/daicarjim/system-rossy-frontend-update/-/blob/master/pantalla%20system%20rossy.png)
## Technologies
***
Una lista de tecnologias utilizadas en el proyecto:
* [Node JS](https://nodejs.org/en): Version 16.3 
* [Apollo Client](https://www.apollographql.com/docs/react/): Version 2.11.0
* [Next JS](https://nextjs.org/): Version 29.4.3
* [Formik](https://formik.org/): Version 5.9.7
* [Sweetalert2](https://sweetalert2.github.io/): Version 5.9.7
## Installation
***
Los primeros pasos para instalacion y correr el proyecto en local. 
```
$ git clone https://gitlab.com/daicarjim/system-rossy-frontend-update
$ npm i
$ npm run dev
```